from fastapi import FastAPI,File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
import matplotlib.pyplot as plt
import numpy as np
import io
import cv2
import tensorflow as tf
import os
from tensorflow.keras import preprocessing
from PIL import Image
from model import generator
from io import BytesIO
from starlette.responses import StreamingResponse
import base64

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# file_name = r'C:\Users\talta\Desktop\projects\genroom\server_side\gan.h5'
file_name= "generator.h5"
# print(gan)
generator.load_weights(file_name)
# with h5py.File(file_name,'w') as f:
    # model = tf.keras.models.load_model(f)

@app.get("/")
async def root():
    return {"message": "Queen T!"}

def read_imagefile(file) -> Image.Image:
    image = Image.open(BytesIO(file))
    return image

@app.post("/predict")
async def predict_api(file: UploadFile = File(...)):
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "Image must be jpg or png format!"
    # print("before read image")
    image = read_imagefile(await file.read())
    image_array  = tf.keras.preprocessing.image.img_to_array(image)
    image_array.resize(256,256,3)
    gray_img = cv2.cvtColor(image_array, cv2.COLOR_BGR2GRAY)
    gray_inverse = 255-gray_img
    sigma = 1*1.5
    blurred_img = cv2.GaussianBlur(gray_inverse, (51, 51), sigmaX=sigma, sigmaY=sigma)
    blurred_inverse = 255 - blurred_img
    sketchy_img = cv2.divide(gray_img, blurred_inverse, scale=256.0)
    sketchy_img[np.isnan(sketchy_img)] = 0
    sketchy_img = sketchy_img.reshape((256,256,1))
    pred = np.array([sketchy_img])

    prediction = generator(pred)
    
    prediction = prediction[0] * 255
    temp = prediction.numpy()
    cv2.imwrite("./temp.jpeg", temp)
    prediction = tf.strings.as_string(prediction)
    a = tf.io.decode_base64(
        prediction,
        name=None
    )
    base64_image = base64.b64decode(a)
   
    print(base64_image)
    return 