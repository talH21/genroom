from numpy.random import randint
from tensorflow import keras
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.initializers import RandomNormal
from tensorflow.keras.models import Model
from tensorflow.python.keras.models import Input
from tensorflow.keras.layers import InputLayer
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Conv2DTranspose
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf

image_shape=(256,256,3)
init = RandomNormal(stddev=0.02)
discriminator = keras.models.Sequential()
# [
discriminator.add(InputLayer(input_shape=(image_shape)))
discriminator.add(Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init))
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init))
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init))
discriminator.add(BatchNormalization())
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(256, (4,4), strides=(2,2), padding='same', kernel_initializer=init))
discriminator.add(BatchNormalization())
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init))
discriminator.add(BatchNormalization())
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(512, (4,4), padding='same', kernel_initializer=init))
discriminator.add(BatchNormalization())
discriminator.add(LeakyReLU(alpha=0.2))
discriminator.add(Conv2D(1, (4,4), padding='same', kernel_initializer=init))
discriminator.add(Flatten())
discriminator.add(keras.layers.Dropout(0.2))
discriminator.add(keras.layers.Dense(64, activation=keras.layers.LeakyReLU(alpha=0.01)))
discriminator.add(keras.layers.Dense(1, activation="sigmoid"))
# ])

opt = Adam(lr=0.0002, beta_1=0.5)
discriminator.compile(loss='binary_crossentropy', optimizer=opt, loss_weights=[0.5], metrics=['accuracy'])

def define_encoder_block(layer_in, n_filters, batchnorm=True):
    init = RandomNormal(stddev=0.02)
    g = Conv2D(n_filters, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(layer_in)
    if batchnorm:
        g = BatchNormalization()(g, training=True)
    g = LeakyReLU(alpha=0.2)(g)
    return g

# define a decoder block
def decoder_block(layer_in, skip_in, n_filters, dropout=True):
    init = RandomNormal(stddev=0.02)
    g = Conv2DTranspose(n_filters, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(layer_in)
    g = BatchNormalization()(g, training=True)
    if dropout:
        g = Dropout(0.5)(g, training=True)
    g = Concatenate()([g, skip_in])
    g = Activation('relu')(g)
    return g

init = RandomNormal(stddev=0.02)
in_image = Input(shape=(256,256,1))

# encoder model
e1 = define_encoder_block(in_image, 64, batchnorm=False)
e2 = define_encoder_block(e1, 128)
e3 = define_encoder_block(e2, 256)
e4 = define_encoder_block(e3, 512)
e5 = define_encoder_block(e4, 512)
e6 = define_encoder_block(e5, 512)
e7 = define_encoder_block(e6, 512)

# bottleneck, no batch norm and relu
b = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(e7)
b = Activation('relu')(b)

# decoder model
d1 = decoder_block(b, e7, 512)
d2 = decoder_block(d1, e6, 512)
d3 = decoder_block(d2, e5, 512)
d4 = decoder_block(d3, e4, 512, dropout=False)
d5 = decoder_block(d4, e3, 256, dropout=False)
d6 = decoder_block(d5, e2, 128, dropout=False)
d7 = decoder_block(d6, e1, 64, dropout=False)

g = Conv2DTranspose(3, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(d7)

out_image = Activation('tanh')(g)

generator = Model(in_image, out_image)

for layer in discriminator.layers:
    if not isinstance(layer, BatchNormalization):
        layer.trainable = False

in_src = Input(shape=(256,256,1))

# generator = Unet()

gen_out = generator(in_src)

dis_out = discriminator(gen_out)

gan = Model(in_src, [gen_out, dis_out])
opt = Adam(lr=0.0002, beta_1=0.5)
gan.compile(loss=['mse','binary_crossentropy'], optimizer=opt, loss_weights=[0.99,0.01], metrics=['accuracy'])